# __author__ "Gabriela Viñan"
# __email__ gabriela.vinan@unl.edu.ec
"""Crear un programa en el cual se permita crear una actividad que contenga al menos 3 preguntas de
selección en base al modelo de clases codificado."""
while True:
    class Actividad:
        identificador = str
        nombre = str
        preguntas = []
        def __init__(self,identificador,nombre):
            self.identificador = identificador
            self.nombre = nombre

    class PreguntaSeleccion(Actividad):
        numero = int
        text = str
        op1 = str
        op2 = str
        op3 = str
        resp = str

        def __init__(self, numero,text, op1, op2, op3, resp):
            self.numero = numero
            self.text = text
            self.op1 = op1
            self.op2 = op2
            self.op3 = op3
            self.resp = resp

    idenfificador = input("Ingrese el identificador: ")
    nombre = input("Ingrese el nombre: ")

    numero = input("Ingrese el número de pregunta: ")
    texto = input("Ingrese la pregunta: ")
    op1 = input("Ingrese la opción 1: ")
    op2 = input("Ingrese la opción 2: ")
    op3 = input("Ingrese la opción 3: ")
    resp = input("Ingrese la respuesta correcta: ")

    listaPreguntas = []
    componente = Actividad(idenfificador, nombre)
    p = PreguntaSeleccion(numero, texto, op1, op2, op3, resp)
    p = PreguntaSeleccion(numero, texto, op1, op2, op3, resp)
    listaPreguntas.append(p)

    print('Identificador componente:',componente.identificador,'\n','Nombre componente: ',componente.nombre,'\n')
    print('Número: %s \nTexto: %s \nOpcion1: %s \nOpcion2: %s \nOpcion3: %s. \nRespuesta correcta: %s'%(p.numero, p.text, p.op1, p.op2, p.op3, p.resp))


